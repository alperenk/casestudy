﻿using System;
using FluentAssertions;
using MarsRover.Console.Service;
using MarsRover.Console.Service.Implementations;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace MarsRover.Tests
{
    public class InstructionTests
    {
        private readonly ILogger<InstructionService> _loggerMock;
        private IInstructionService _instructionService;
        public InstructionTests()
        {
            _loggerMock = Mock.Of<ILogger<InstructionService>>();
        }

        [Theory]
        [InlineData(null)]
        public void GetInstructions_IsNull_ThrowsValidatePlateauException(string instruction)
        {
            _instructionService = new InstructionService(_loggerMock);

            Assert.Throws<ArgumentNullException>(() => _instructionService.GetInstructions(instruction));
        }

        [Theory]
        [InlineData("LMLMLMLMM")]
        [InlineData("MMRMMRMRRM")]
        public void GetInstructions_IsNotNullAndValidInstruction_ShouldReturnInstructionList_CountShouldEqualWithInstructionLength(string instruction)
        {
            _instructionService = new InstructionService(_loggerMock);

            var baseInstructionModel = _instructionService.GetInstructions(instruction);

            baseInstructionModel.Data.Count.Should().Equals(instruction.Length);
        }
    }
}