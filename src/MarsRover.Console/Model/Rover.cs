﻿using System;

namespace MarsRover.Console.Model
{
    public class Rover
    {
        public Rover()
        {
            Location = new RoverLocation();
        }
        public Guid Id { get; set; }
        public RoverLocation Location { get; set; }
    }
}
