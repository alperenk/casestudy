﻿using MarsRover.Console.Common.Enum;

namespace MarsRover.Console.Model
{
   public class RoverLocation
    {
        public int XPosition { get; set; }
        public int YPosition { get; set; }
        public Direction Direction { get; set; }
    }
}
