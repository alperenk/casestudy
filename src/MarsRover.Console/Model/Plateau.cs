﻿namespace MarsRover.Console.Model
{
    public class Plateau
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
