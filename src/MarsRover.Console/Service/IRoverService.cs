﻿using MarsRover.Console.Common.Enum;
using MarsRover.Console.Model;

namespace MarsRover.Console.Service
{
   public interface IRoverService
    {
        BaseModel<Rover> Initialize(Plateau plateauModel, RoverLocation roverLocation);
        BaseModel<bool> Validate(Plateau plateauModel, RoverLocation roverLocation);
        BaseModel<Rover> GetCurrentRover();
        void ExplorePlateau(Instruction instruction);
    }
}
