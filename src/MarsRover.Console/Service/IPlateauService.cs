﻿using MarsRover.Console.Model;

namespace MarsRover.Console.Service
{
   public interface IPlateauService
    {
        BaseModel<Plateau> Create(int width, int height);
        BaseModel<bool> Validate(Plateau plateau);
    }
}
