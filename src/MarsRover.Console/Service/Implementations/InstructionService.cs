﻿using System;
using System.Collections.Generic;
using MarsRover.Console.Common.Enum;
using MarsRover.Console.Common.Exception;
using MarsRover.Console.Model;
using Microsoft.Extensions.Logging;

namespace MarsRover.Console.Service.Implementations
{
    public class InstructionService : IInstructionService
    {
        private readonly ILogger<InstructionService> _logger;
        public InstructionService(ILogger<InstructionService> logger)
        {
            _logger = logger;
        }
        public BaseModel<List<Instruction>> GetInstructions(string commands)
        {
            var baseInstructionModel = new BaseModel<List<Instruction>>
            {
                Data = new List<Instruction>()
            };
            
            if (!string.IsNullOrEmpty(commands))
            {
                var instructions = commands.ToCharArray();
                foreach (var instruction in instructions)
                {
                    switch (instruction)
                    {
                        case (char)Instruction.RotateRight:
                            baseInstructionModel.Data.Add(Instruction.RotateRight);
                            break;
                        case (char)Instruction.RotateLeft:
                            baseInstructionModel.Data.Add(Instruction.RotateLeft);
                            break;
                        case (char)Instruction.MoveForward:
                            baseInstructionModel.Data.Add(Instruction.MoveForward);
                            break;
                        default:
                            _logger.LogCritical("Invalid instruction.");
                            throw new InvalidInstructionException();
                    }
                }
            }
            else
            {
                _logger.LogError("Instructions not found.");
                throw new ArgumentNullException();
            }

            _logger.LogInformation("Instructions settled successfully.");
            return baseInstructionModel;
        }
    }
}
