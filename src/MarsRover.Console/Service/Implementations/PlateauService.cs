﻿using MarsRover.Console.Common.Exception;
using MarsRover.Console.Model;
using Microsoft.Extensions.Logging;

namespace MarsRover.Console.Service.Implementations
{
    public class PlateauService : IPlateauService
    {
        private readonly ILogger<PlateauService> _logger;

        public PlateauService(ILogger<PlateauService> logger)
        {
            _logger = logger;
        }

        public BaseModel<Plateau> Create(int width, int height)
        {
            var model = new BaseModel<Plateau>();

            var plateauModel = new Plateau()
            {
                Width = width,
                Height = height
            };

            var validationResponse = Validate(plateauModel);

            if (validationResponse.Data)
            {
                model.Data = plateauModel;
                _logger.LogInformation("Plateau created successfully.");
            }

            return model;
        }

        public BaseModel<bool> Validate(Plateau plateau)
        {
            var model = new BaseModel<bool>();

            if (plateau != null && plateau.Width > 0 && plateau.Height > 0)
            {
                model.Data = true;
                _logger.LogInformation($"Plateau has valid size ({plateau.Width}-{plateau.Height}).");
            }
            else
            {
                var exception = new ValidatePlateauException();
                _logger.LogError(exception.Message);
                throw exception;
            }

            return model;
        }
    }
}