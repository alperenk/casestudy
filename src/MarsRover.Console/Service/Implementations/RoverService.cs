﻿using System;
using MarsRover.Console.Model;
using MarsRover.Console.Common.Enum;
using MarsRover.Console.Common.Exception;
using MarsRover.Console.Core.Factory;
using Microsoft.Extensions.Logging;

namespace MarsRover.Console.Service.Implementations
{
    public class RoverService : IRoverService
    {
        private Rover CurrentRover { get; set; }
        private readonly ILogger<RoverService> _logger;
        private readonly IExplorationFactory _explorationFactory;
        public RoverService(ILogger<RoverService> logger, IExplorationFactory explorationFactory)
        {
            _logger = logger;
            _explorationFactory = explorationFactory;
        }

        public BaseModel<Rover> Initialize(Plateau plateauModel, RoverLocation roverLocation)
        {
            var model = new BaseModel<Rover>();

            var validateLocationModel = Validate(plateauModel, roverLocation);

            if (validateLocationModel.Data)
            {
                model.Data = new Rover
                {
                    Id = Guid.NewGuid(),
                    Location =
                    {
                        Direction = roverLocation.Direction,
                        XPosition = roverLocation.XPosition,
                        YPosition = roverLocation.YPosition
                    }
                };
                CurrentRover = model.Data;
                _logger.LogInformation("Rover initialized successfully.");
            }

            return model;
        }
        public BaseModel<bool> Validate(Plateau plateauModel, RoverLocation roverLocation)
        {
            var model = new BaseModel<bool>();

            var isValidDirection = IsValidDirection(roverLocation.Direction);
            var isValidXPosition = IsValidXPosition(plateauModel.Width, roverLocation.XPosition);
            var isValidYPosition = IsValidYPosition(plateauModel.Height, roverLocation.YPosition);

            if (isValidDirection && isValidXPosition && isValidYPosition)
            {
                model.Data = true;
                _logger.LogInformation($"Given location ({roverLocation.XPosition}-{roverLocation.YPosition}-{roverLocation.Direction}) for the rover is valid on the given plateau ({plateauModel.Width}-{plateauModel.Height}).");
            }
            else
            {
                var exception = new ValidateRoverLocationException();
                _logger.LogError(exception.Message);
                throw exception;
            }

            return model;
        }

        public BaseModel<Rover> GetCurrentRover()
        {
            var model = new BaseModel<Rover>();

            if (CurrentRover != null)
            {
                model.Data = CurrentRover;
            }
            else
            {
                model.Errors.Add("Rover not found.");
            }

            return model;
        }

        public void ExplorePlateau(Instruction instruction)
        {
            _explorationFactory.ExecuteInstruction(instruction).Explore(CurrentRover.Location);

            _logger.LogInformation($"Rover location is ({CurrentRover.Location.XPosition}-{CurrentRover.Location.YPosition}-{CurrentRover.Location.Direction})");
        }

        private bool IsValidDirection(Direction direction)
        {
            if (direction == Direction.West ||
                direction == Direction.East ||
                direction == Direction.North ||
                direction == Direction.South)
            {
                return true;
            }

            return false;
        }
        private bool IsValidXPosition(int width, int xPosition)
        {
            return xPosition >= 0 && xPosition <= width;
        }
        private bool IsValidYPosition(int height, int yPosition)
        {
            return yPosition >= 0 && yPosition <= height;
        }
    }
}