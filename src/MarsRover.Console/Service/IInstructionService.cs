﻿using System.Collections.Generic;
using MarsRover.Console.Common.Enum;
using MarsRover.Console.Model;

namespace MarsRover.Console.Service
{
    public interface IInstructionService
    {
        BaseModel<List<Instruction>> GetInstructions(string instruction);
    }
}
