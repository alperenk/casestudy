using MarsRover.Console.Model;

namespace MarsRover.Console.Core.Command
{
    public interface IExplorationCommand
    {
        void Explore(RoverLocation roverLocation);
    }
}