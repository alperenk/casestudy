using MarsRover.Console.Common.Enum;
using MarsRover.Console.Core.Command;

namespace MarsRover.Console.Core.Factory
{
    public interface IExplorationFactory
    {
        IExplorationCommand ExecuteInstruction(Instruction instruction);
    }
}