using MarsRover.Console.Common.Enum;
using MarsRover.Console.Core.Command;

namespace MarsRover.Console.Core.Factory
{
    public class ExplorationFactory : IExplorationFactory
    {
        public IExplorationCommand ExecuteInstruction(Instruction instruction)
        {
            IExplorationCommand explorationCommand = null;

            switch (instruction)
            {
                case Instruction.RotateRight:
                    explorationCommand = new RotateRightCommand();
                    break;
                case Instruction.RotateLeft:
                    explorationCommand = new RotateLeftCommand();
                    break;
                case Instruction.MoveForward:
                    explorationCommand = new MoveForwardCommand();
                    break;
            }

            return explorationCommand;
        }
    }
}