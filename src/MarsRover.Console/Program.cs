﻿using System;
using System.Collections.Generic;
using MarsRover.Console.Common.Enum;
using MarsRover.Console.Core.Command;
using MarsRover.Console.Core.Factory;
using MarsRover.Console.Model;
using MarsRover.Console.Service;
using MarsRover.Console.Service.Implementations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace MarsRover.Console
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Configuration();
        }

        private static void Configuration()
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging((opt =>
                {
                    opt.SetMinimumLevel(LogLevel.Debug);
                    opt.AddConsole();
                    opt.AddDebug();
                }))
                .AddSingleton<IExplorationFactory, ExplorationFactory>()
                //commands
                .AddSingleton<IExplorationCommand, RotateRightCommand>()
                .AddSingleton<IExplorationCommand, RotateLeftCommand>()
                .AddSingleton<IExplorationCommand, MoveForwardCommand>()
                //services
                .AddSingleton<IPlateauService, PlateauService>()
                .AddSingleton<IRoverService, RoverService>()
                .AddSingleton<IInstructionService, InstructionService>()
                .BuildServiceProvider();

            serviceProvider.GetService<ILoggerFactory>().CreateLogger<Program>();
            
            Initialize(serviceProvider);
        }

        private static void Initialize(IServiceProvider serviceProvider)
        {
            var plateauService = serviceProvider.GetService<IPlateauService>();
            var roverService = serviceProvider.GetService<IRoverService>();
            var instructionService = serviceProvider.GetService<IInstructionService>();

            var rovers = new List<Rover>();

            var basePlateau = plateauService.Create(5, 5);
            var firstRoverLocation = new RoverLocation
            {
                XPosition = 1,
                YPosition = 2,
                Direction = Direction.North
            };

            var baseRover = roverService.Initialize(basePlateau.Data, firstRoverLocation);
            rovers.Add(baseRover.Data);

            var baseInstruction = instructionService.GetInstructions("LMLMLMLMM");

            foreach (var instruction in baseInstruction.Data)
            {
                roverService.ExplorePlateau(instruction);
            }

            var secondRoverLocation = new RoverLocation
            {
                XPosition = 3,
                YPosition = 3,
                Direction = Direction.East
            };

            baseRover = roverService.Initialize(basePlateau.Data, secondRoverLocation);
            rovers.Add(baseRover.Data);

            baseInstruction = instructionService.GetInstructions("MMRMMRMRRM");

            foreach (var instruction in baseInstruction.Data)
            {
                roverService.ExplorePlateau(instruction);
            }
        }
    }
}
