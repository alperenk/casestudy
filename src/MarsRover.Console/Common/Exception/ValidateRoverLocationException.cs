namespace MarsRover.Console.Common.Exception
{
    public class ValidateRoverLocationException : System.Exception
    {
        public ValidateRoverLocationException() : base(@"Rover location not validated on the plateau with given parameters.")
        {

        }
    }
}