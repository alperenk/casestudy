namespace MarsRover.Console.Common.Exception
{
    public class ValidatePlateauException : System.Exception
    {
        public ValidatePlateauException():base(@"Plateau not validated with given parameters.")
        {
            
        }
    }
}