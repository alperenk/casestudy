namespace MarsRover.Console.Common.Exception
{
    public class InvalidInstructionException : System.Exception
    {
        public InvalidInstructionException() : base(@"Invalid instruction.")
        {

        }
    }
}