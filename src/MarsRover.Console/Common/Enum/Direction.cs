namespace MarsRover.Console.Common.Enum
{
    public enum Direction
    {
        North = 'N',
        West = 'W',
        South = 'S',
        East = 'E'
    }
}