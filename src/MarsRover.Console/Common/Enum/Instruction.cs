namespace MarsRover.Console.Common.Enum
{
    public enum Instruction
    {
        RotateRight = 'R',
        RotateLeft = 'L',
        MoveForward = 'M'
    }
}